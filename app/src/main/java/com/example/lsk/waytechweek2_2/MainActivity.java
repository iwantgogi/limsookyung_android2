package com.example.lsk.waytechweek2_2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ImageButton toogleNaviButton;
    private Context mContext;
    private ArrayList<ImageButton> imageButtons;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);

        setTitle("한국산업기술대");
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        imageButtons = new ArrayList<>();
        mContext = MainActivity.this;
        setImageAlpha();
        toogleNaviButton= (ImageButton)findViewById(R.id.naviagtion_bar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        imageButtons.add((ImageButton) findViewById(R.id.imageButton));
        imageButtons.add((ImageButton) findViewById(R.id.imageButton2));
        imageButtons.add((ImageButton) findViewById(R.id.imageButton3));
        imageButtons.add((ImageButton) findViewById(R.id.imageButton4));
        imageButtons.add((ImageButton) findViewById(R.id.imageButton5));
        imageButtons.add((ImageButton) findViewById(R.id.imageButton6));

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id=v.getId();
                switch (id){

                    case R.id.imageButton:
                        Intent intent = new Intent(mContext,Activity1.class);
                        startActivity(intent);
                        finish();
                    case R.id.imageButton2:
                        Intent intent2 = new Intent(mContext,Activity1.class);
                        startActivity(intent2);
                        finish();
                    case R.id.imageButton3:
                        Intent intent3 = new Intent(mContext,Activity1.class);
                        startActivity(intent3);
                        finish();
                    case R.id.imageButton4:
                        Intent intent4 = new Intent(mContext,Activity1.class);
                        startActivity(intent4);
                        finish();
                    case R.id.imageButton5:
                        Intent intent5 = new Intent(mContext,Activity1.class);
                        startActivity(intent5);
                        finish();
                    case R.id.imageButton6:
                        Intent intent6 = new Intent(mContext,Activity1.class);
                        startActivity(intent6);
                        finish();
                    default:
                        break;
                }
            }
        };
    }
    private void setImageAlpha() {
        for (int i = 0; i < imageButtons.size(); i++) {
            imageButtons.get(i).setAlpha((float) 0.8);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if(drawer.isDrawerOpen(Gravity.LEFT)){
            drawer.closeDrawer(Gravity.LEFT);
        }
        else {
            super.onBackPressed();
        }
    }
    }